package com.slm.slm1;

import android.content.Context;

import androidx.test.platform.app.InstrumentationRegistry;

import com.slm.slm1.dao.LocationDao;
import com.slm.slm1.dao.LocationDetailsDao;
import com.slm.slm1.dao.TaskDao;
import com.slm.slm1.dao.TaskDetailsDao;
import com.slm.slm1.helper.AppDatabase;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskAndTaskDetails;
import com.slm.slm1.model.TaskDetails;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AppDatabaseTest {

    TaskDao mTaskDao;
    TaskDetailsDao mTaskDetailsDao;
    LocationDao mLocationDao;
    LocationDetailsDao mLocationDetailsDao;

    @Before
    public void setUp() throws Exception {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("com.slm.slm1", appContext.getPackageName());

        AppDatabase.setContext(appContext);

        mTaskDao = AppDatabase.getInstance().taskDao();
        mTaskDetailsDao = AppDatabase.getInstance().taskDetailsDao();
        mLocationDao = AppDatabase.getInstance().locationDao();
        mLocationDetailsDao = AppDatabase.getInstance().locationDetailsDao();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void clearDB()
    {
        mLocationDao.nukeTable(); // TODO:
    }

    @Test
    public void taskDao() {
        TaskDao taskDao = AppDatabase.getInstance().taskDao();
        TaskDetailsDao taskDetailsDao = AppDatabase.getInstance().taskDetailsDao();

        Task task = new Task();
        long rowId = taskDao.insertTask(task);

        TaskDetails taskDetails = new TaskDetails();
        taskDetails.name = "TestName";
        taskDetails.desc = "TestDesc";
        taskDetails.taskfk = (int)rowId;
        taskDetailsDao.insertTaskDetails(taskDetails);

        List<TaskAndTaskDetails> tasks = taskDao.getTasksAndTaskDetails();

        TaskAndTaskDetails taskInfo = tasks.get((int)rowId - 1);
        assertTrue(taskInfo.taskDetails != null);
    }

    @Test
    public void taskDetailsDao() {
        TaskDetailsDao taskDetailsDao = AppDatabase.getInstance().taskDetailsDao();
    }

    @Test
    public void locationDao() {
    }

    @Test
    public void locationDetailsDao() {
    }

    @Test
    public void recordDao() {
    }
}