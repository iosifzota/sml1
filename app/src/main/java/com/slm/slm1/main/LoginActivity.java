package com.slm.slm1.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.slm.slm1.R;
import com.slm.slm1.dao.LocationDao;
import com.slm.slm1.dao.TaskDao;
import com.slm.slm1.dao.TaskDetailsDao;
import com.slm.slm1.helper.AppConfig;
import com.slm.slm1.helper.AppDatabase;
import com.slm.slm1.helper.AuthInformation;
import com.slm.slm1.helper.LoggedUserInformation;
import com.slm.slm1.helper.LoginRequest;
import com.slm.slm1.helper.LoginResponse;
import com.slm.slm1.helper.RegisterRequest;
import com.slm.slm1.helper.RegisterResponse;
import com.slm.slm1.helper.RestoreRequest;
import com.slm.slm1.helper.ServerConnection;
import com.slm.slm1.helper.ServerConnectionRunner;
import com.slm.slm1.helper.ServerConnectionRunnerManager;
import com.slm.slm1.helper.TaskSerializable;
import com.slm.slm1.helper.UserCredentials;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskDetails;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.List;

public class LoginActivity extends AppCompatActivity {
    private EditText edUsername, edPassword;
    private AttemptLoginTask attemptLoginTask;
    private AttemptRegisterTask attemptRegisterTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edUsername = findViewById(R.id.edUsername);
        edPassword = findViewById(R.id.edPassword);
    }

    private void continueLoginActivity() {
        if (LoggedUserInformation.getInstance().isUserLogged()) {
            UserCredentials userCredentials = new UserCredentials();
            userCredentials.username = LoggedUserInformation.getInstance().getUser().username;
            userCredentials.password = LoggedUserInformation.getInstance().getUser().password;

            attemptLoginTask = new AttemptLoginTask(userCredentials);
            attemptLoginTask.execute();
        }
    }

    public class AttemptLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final UserCredentials userCredentials;
        private Socket socket = null;

        public AttemptLoginTask(UserCredentials userCredentials) {
            this.userCredentials = userCredentials;
        }

        @Override
        public Boolean doInBackground(Void... voids) {
            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(AppConfig.HOST, 3000), 3000);

                if (socket.isConnected())
                    socket.setSoTimeout(5000);
                else
                    return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            ServerConnection serverConnection = new ServerConnection(socket);
            boolean loginSuccessful = false;

            LoginRequest loginRequest = new LoginRequest();
            loginRequest.userCredentials = userCredentials;

            if (serverConnection.writeObject(loginRequest)) {
                {
                    LoginResponse loginResponse = (LoginResponse) (serverConnection.readObject());
                    if (loginResponse != null) {
                        loginSuccessful = loginResponse.status;
                    }
                }
            }

            serverConnection.closeConnection();
            return loginSuccessful;
        }

        @Override
        public void onPostExecute(Boolean loginSucceeded) {
            if (loginSucceeded) {
                AuthInformation.createAuthInformation(userCredentials);
                onSuccessfullyLoggedIn();
            } else if (socket.isConnected()) {
                Toast.makeText(getApplicationContext(), "Login credentials are wrong", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Failed to connect to server.", Toast.LENGTH_LONG).show();
            }
        }
    }

    public class AttemptRegisterTask extends AsyncTask<Void, Void, Boolean> {

        private final UserCredentials userCredentials;
        private Socket socket;

        public AttemptRegisterTask(UserCredentials userCredentials) {
            this.userCredentials = userCredentials;
        }

        @Override
        public Boolean doInBackground(Void... voids) {
            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(AppConfig.HOST, 3004), 3000);

                if (socket.isConnected())
                    socket.setSoTimeout(5000);
                else
                    return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            ServerConnection serverConnection = new ServerConnection(socket);
            boolean success = false;

            RegisterRequest registerRequest = new RegisterRequest();
            registerRequest.userCredentials = userCredentials;

            if (serverConnection.writeObject(registerRequest)) {

                RegisterResponse registerResponse = (RegisterResponse) serverConnection.readObject();

                if (registerResponse != null) {
                    success = registerResponse.success;
                }
            }

            serverConnection.closeConnection();
            return success;
        }

        @Override
        public void onPostExecute(Boolean success) {
            if (success) {
                AuthInformation.createAuthInformation(userCredentials);
                onSuccessfullyLoggedIn();
            } else if (socket.isConnected()) {
                Toast.makeText(getApplicationContext(), "Failed to register.", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Failed to connect to server.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void onLogin(View view) {
        UserCredentials userCredentials = readUserCredentials();
        if (attemptLoginTask == null || attemptLoginTask.getStatus() != AsyncTask.Status.RUNNING) {
            attemptLoginTask = new AttemptLoginTask(userCredentials);
            attemptLoginTask.execute();
        } else {
            Toast.makeText(this, "Please wait.", Toast.LENGTH_SHORT).show();
        }
    }

    public void onRegister(View view) {
        UserCredentials userCredentials = readUserCredentials();
        if (attemptRegisterTask == null || attemptRegisterTask.getStatus() != AsyncTask.Status.RUNNING) {
            attemptRegisterTask = new AttemptRegisterTask(userCredentials);
            attemptRegisterTask.execute();
        } else {
            Toast.makeText(this, "Please wait.", Toast.LENGTH_SHORT).show();
        }
    }

    public UserCredentials readUserCredentials() {
        UserCredentials userCredentials = new UserCredentials();

        userCredentials.username = edUsername.getText().toString();
        userCredentials.password = edPassword.getText().toString();

        return userCredentials;
    }

    public void onSuccessfullyLoggedIn() {
        Thread thread = new Thread(new ServerConnectionRunner(getApplicationContext()));
        ServerConnectionRunnerManager.createUnique(thread).start();
        finish();
    }


}
