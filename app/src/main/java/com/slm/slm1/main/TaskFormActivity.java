package com.slm.slm1.main;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.model.LatLng;
import com.slm.slm1.R;
import com.slm.slm1.dao.LocationDao;
import com.slm.slm1.dao.TaskDao;
import com.slm.slm1.dao.TaskDetailsDao;
import com.slm.slm1.helper.AppConfig;
import com.slm.slm1.helper.AppDatabase;
import com.slm.slm1.helper.AuthInformation;
import com.slm.slm1.helper.DatabaseUtil;
import com.slm.slm1.helper.SaveResponse;
import com.slm.slm1.helper.ServerConnection;
import com.slm.slm1.helper.TaskSerializable;
import com.slm.slm1.helper.TasksToSaveManager;
import com.slm.slm1.helper.TasksToUpdateManager;
import com.slm.slm1.helper.UserCredentials;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskAndTaskDetails;
import com.slm.slm1.model.TaskDetails;

import java.io.IOException;
import java.net.Socket;
import java.nio.channels.AsynchronousChannelGroup;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class TaskFormActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {

    TextView edLat, edLg, edNotes, txtDeadline;
    EditText edName;

    TaskAndTaskDetails mTask;
    Location mLocation;
    LatLng mLocationCoordinates;

    ToggleButton tglTaskStatus;
    private static SaveTaskTask saveTaskTask;
    private static UpdateTasksTask updateTasksTask;
    private Date deadline;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_form);

        Intent intent = getIntent();
        // check what happens if activity is called with no intent

        int taskid = intent.getIntExtra("taskid", -1);

        edNotes = findViewById(R.id.edNotes);
        edName = findViewById(R.id.edName);
        edLat  = findViewById(R.id.edLat);
        edLg   = findViewById(R.id.edLg);
        tglTaskStatus = findViewById(R.id.tglTaskStatus);
        txtDeadline = findViewById(R.id.txtDeadline);

        tglTaskStatus.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (mTask == null) return;

                if (isChecked) {
                    mTask.task.completionStatus = true;
                } else {
                    mTask.task.completionStatus = false;
                }

            }
        });

        LocationDao locationDao = AppDatabase.getInstance().locationDao();
        TaskDao taskDao = AppDatabase.getInstance().taskDao();

        if (taskid != -1) {
            mTask = taskDao.getTaskAndTaskDetails(taskid);
            mLocation = locationDao.getLocation(mTask.task.locationfk);

            edNotes.setText(mTask.taskDetails.notes);
            edName.setText(mTask.task.name);
            tglTaskStatus.setChecked(mTask.task.completionStatus);
            int year  = mTask.task.deadline.getYear();
            int month = mTask.task.deadline.getMonth();
            int day   = mTask.task.deadline.getDate();
            deadline = new Date(year, month, day);
            txtDeadline.setText(String.format("%d/%d/%d", day, month, year));
        } else {
            int year = Calendar.getInstance().get(Calendar.YEAR);
            int month = Calendar.getInstance().get(Calendar.MONTH) + 1;
            int day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
            deadline = new Date(year, month, day);
            txtDeadline.setText(String.format("%d/%d/%d", deadline.getDate(), deadline.getMonth(), deadline.getYear()));
            mLocationCoordinates = intent.getParcelableExtra("location");
            mLocation = locationDao.find(mLocationCoordinates.latitude, mLocationCoordinates.longitude);
        }

        if (mLocation != null) {
            edLat.setText(Double.toString(mLocation.lat));
            edLg.setText(Double.toString(mLocation.lg));
        }

        if (saveTaskTask == null) {
            saveTaskTask = new SaveTaskTask();
        }

        if (updateTasksTask == null) {
            updateTasksTask = new UpdateTasksTask();
        }
    }

    public void insertNewTask() {
        if (mTask == null) {
            TaskDao taskDao = AppDatabase.getInstance().taskDao();
            TaskDetailsDao taskDetailsDao = AppDatabase.getInstance().taskDetailsDao();

            if (mLocation == null) {
                LocationDao locationDao = AppDatabase.getInstance().locationDao();
                locationDao.insert(new Location(mLocationCoordinates.latitude, mLocationCoordinates.longitude));
                mLocation = locationDao.find(mLocationCoordinates.latitude, mLocationCoordinates.longitude);
            }

            Task task = new Task();
            task.name = edName.getText().toString();
            task.locationfk = mLocation.id;
            long rowId = taskDao.insertTask(task);

            TaskDetails taskDetails = new TaskDetails();
            taskDetails.notes = edNotes.getText().toString();
            taskDetails.taskfk = (int)rowId;
            taskDetailsDao.insertTaskDetails(taskDetails);

            mTask = taskDao.getTaskAndTaskDetails(taskDetails.taskfk);

            TaskSerializable taskSerializable = new TaskSerializable();
            taskSerializable.taskName = task.name;
            taskSerializable.taskNotes = taskDetails.notes;
            taskSerializable.completionStatus = task.completionStatus;
            taskSerializable.locationLat = mLocation.lat;
            taskSerializable.locationLg = mLocation.lg;
            taskSerializable.targetUsername = AuthInformation.getInstance().getUserCredentials().username;
            taskSerializable.localTaskId = taskDetails.taskfk;
            taskSerializable.deadline = deadline;

            mTask.task.serverTaskId = -1;
            taskDao.update(mTask.task);

            TasksToSaveManager.getInstance().addTasksToSave(taskSerializable);
            // save the task on the server
            if (saveTaskTask.getStatus() != AsyncTask.Status.RUNNING) {
                saveTaskTask = new SaveTaskTask();
                saveTaskTask.execute(TasksToSaveManager.getInstance().getTasksToSave());
            } else {
                throw new RuntimeException("Task is already running");
            }
        }
    }

    public void onSave(View view) {
        if (deadline == null) {
            Toast.makeText(this, "Deadline is mandatory.", Toast.LENGTH_SHORT).show();
            return;
        }

        if (mTask == null) {
            insertNewTask();
        }

        mTask.task.name = edName.getText().toString();
        mTask.task.deadline = deadline;
        mTask.taskDetails.notes = edNotes.getText().toString();

        TaskDetailsDao taskDao = AppDatabase.getInstance().taskDetailsDao();
        taskDao.updateTasks(mTask.task);
        taskDao.updateTasks(mTask.taskDetails);

        TaskSerializable taskSerializable = new TaskSerializable();
        taskSerializable.taskName = mTask.task.name;
        taskSerializable.taskNotes = mTask.taskDetails.notes;
        taskSerializable.completionStatus = mTask.task.completionStatus;
        taskSerializable.locationLat = mLocation.lat;
        taskSerializable.locationLg = mLocation.lg;
        taskSerializable.targetUsername = AuthInformation.getInstance().getUserCredentials().username;
        taskSerializable.localTaskId = mTask.task.serverTaskId;
        taskSerializable.deadline = deadline;

        TasksToUpdateManager.getInstance().addTasksToUpdate(taskSerializable);
        // save the task on the server
        if (updateTasksTask.getStatus() != AsyncTask.Status.RUNNING) {
            updateTasksTask = new UpdateTasksTask();
            updateTasksTask.execute();
        } else {
            Toast.makeText(this, "The task is been saved.", Toast.LENGTH_SHORT).show();
        }

        MapActivity.notifyForNotificationUpdate();
        finish();
    }

    public class UpdateTasksTask extends AsyncTask<Void, Void, Boolean> {

        private List<TaskSerializable> tasksToUpdate;

        @Override
        protected Boolean doInBackground(Void... voids) {
            tasksToUpdate = TasksToUpdateManager.getInstance().getTasksToUpdate();

            if (tasksToUpdate != null && !tasksToUpdate.isEmpty()) {
                Socket socket;
                try {
                    socket = new Socket(AppConfig.HOST, 3009);
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }

                ServerConnection serverConnection = new ServerConnection(socket);
                boolean success = false;

                if (serverConnection.writeObject(tasksToUpdate)) {
                    Boolean response = (Boolean) serverConnection.readObject();

                    if (response != null) {
                        success = response;
                    }
                }

                serverConnection.closeConnection();
            }

            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            if (!success) {
                TasksToUpdateManager.getInstance().addTasksToUpdate(tasksToUpdate);
            }
        }
    }

    public void onDelete(View view) {
        finish();
    }

    public void onSendToFriend(View view) {
        if (mTask != null) {
            mTask.task.name = edName.getText().toString();
            mTask.taskDetails.notes = edNotes.getText().toString();
            mTask.task.deadline = deadline;

            TaskDetailsDao taskDao = AppDatabase.getInstance().taskDetailsDao();
            taskDao.updateTasks(mTask.taskDetails);

            Intent intent = new Intent(this, SendTaskActivity.class);
            intent.putExtra("taskid", mTask.task.id);
            startActivityForResult(intent, 0);
        } else {
            Toast.makeText(this, "Save the task first!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 0) {
            boolean isSend = false;
            try {
                isSend = data.getBooleanExtra("isSend", false);
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (isSend) {
                // delete task for current user
                // on the server the task will belong to target user
                TaskDao taskDao = AppDatabase.getInstance().taskDao();
                TaskDetailsDao taskDetailsDao = AppDatabase.getInstance().taskDetailsDao();
                taskDao.deleteTasks(mTask.task);
                taskDetailsDao.deleteTaskDetails(mTask.taskDetails);
                Toast.makeText(this, "Task send successfully", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    public void onShowDatePicker(View view) {
        DialogFragment datePicker = new DatePickerFragment(this);

        datePicker.show(getSupportFragmentManager(), "datePicker");
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        month++;
        txtDeadline.setText(String.format("%d/%d/%d", day, month, year));
        deadline = new Date(year, month, day);
    }


    public class SaveTaskTask extends AsyncTask<List<TaskSerializable>, Void, List<Integer>> {

        private List<TaskSerializable> tasksToSave;

        @Override
        public List<Integer> doInBackground(List<TaskSerializable>... tasks) {
            tasksToSave = tasks[0];
            List<Integer> savedTasksId = new ArrayList<>();

            if (tasksToSave != null && !tasksToSave.isEmpty()) {
                // send tasks
                Socket socket;
                try {
                    socket = new Socket(AppConfig.HOST, 3006);
                } catch (IOException e) {
                    e.printStackTrace();
                    return new ArrayList<>();
                }

                ServerConnection serverConnection = new ServerConnection(socket);

                if (serverConnection.writeObject(tasksToSave)) {
                    // response send
                    List<Integer> response = (List<Integer>) serverConnection.readObject();

                    if (response != null) {
                        savedTasksId = response;
                    }
                }

                serverConnection.closeConnection();
            }

            return savedTasksId;
        }

        @Override
        public void onPostExecute(List<Integer> savedTasksId) {
            if (savedTasksId.isEmpty()) {
                TasksToSaveManager.getInstance().addTasksToSave(tasksToSave);
                tasksToSave.clear();
            } else {
                DatabaseUtil.setServerIdOfTasks(tasksToSave, savedTasksId);
            }
        }
    }


}
