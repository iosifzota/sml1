package com.slm.slm1.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.slm.slm1.R;
import com.slm.slm1.dao.TaskDao;
import com.slm.slm1.helper.AppDatabase;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskAndTaskDetails;

import java.util.ArrayList;
import java.util.List;

public class SearchTasksActivity extends AppCompatActivity {

    EditText edTaskName;
    ListView lstResultTasksList;
    private List<TaskAndTaskDetails> resultTasksList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_tasks);

        edTaskName = findViewById(R.id.edTaskName);
        lstResultTasksList = findViewById(R.id.lstResultTasksList);

        lstResultTasksList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                TaskAndTaskDetails task = resultTasksList.get(position);

                Intent intent = new Intent(getApplicationContext(), TaskFormActivity.class);
                intent.putExtra("taskid", task.task.id);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        resultTasksList = AppDatabase.getInstance().taskDao().getTasksAndTaskDetails();
        updateListing();
    }

    private void updateListing() {
        if (resultTasksList == null) {
            resultTasksList = new ArrayList<>();
        }

        ArrayAdapter<TaskAndTaskDetails> tasksArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, resultTasksList);
        lstResultTasksList.setAdapter(tasksArrayAdapter);
    }

    public void onSearchTask(View view) {
        String taskName = edTaskName.getText().toString();
        TaskDao taskDao = AppDatabase.getInstance().taskDao();
        resultTasksList = taskDao.getTasks(taskName + "%");

        updateListing();
    }
}
