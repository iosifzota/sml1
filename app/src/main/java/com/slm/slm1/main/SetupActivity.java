package com.slm.slm1.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.slm.slm1.R;
import com.slm.slm1.helper.PermissionsActivity;

public class SetupActivity extends AppCompatActivity {

    public boolean permissionsActivityLaunched;
    public boolean loginActivityLaunched;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setup);

        permissionsActivityLaunched = false;
        loginActivityLaunched = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // after permissions have been obtained
        if (!permissionsActivityLaunched && !loginActivityLaunched) {
            permissionsActivityLaunched = true;
            Intent intent = new Intent(this, PermissionsActivity.class);
            startActivity(intent);

        } else if (permissionsActivityLaunched && !loginActivityLaunched) {
            // after user has logged in
            loginActivityLaunched = true;
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            finish();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
