package com.slm.slm1.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.slm.slm1.R;
import com.slm.slm1.helper.AppConfig;
import com.slm.slm1.helper.AuthInformation;
import com.slm.slm1.helper.ServerConnection;
import com.slm.slm1.helper.UpdatePasswordRequest;
import com.slm.slm1.helper.UpdatePasswordResponse;
import com.slm.slm1.helper.UserCredentials;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

public class UserSettingsActivity extends AppCompatActivity {

    EditText edOldPassword, edNewPassword, edConfirmNewPassword;
    TextView txtUsername;
    AttemptPasswordUpdate attemptPasswordUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_settings);

        edOldPassword = findViewById(R.id.edOldPassword);
        edNewPassword = findViewById(R.id.edNewPassword);
        edConfirmNewPassword = findViewById(R.id.edConfirmNewPassword);

        txtUsername = findViewById(R.id.txtUsername);
        txtUsername.setText(AuthInformation.getInstance().getUserCredentials().username);
    }


    public void onChangePassword(View view) {
        String oldPassword = edOldPassword.getText().toString();
        String newPassword = edNewPassword.getText().toString();
        String confirmNewPassword = edConfirmNewPassword.getText().toString();

        if (oldPassword.length() < 1) {
            Toast.makeText(this, "Please fill in the old password.", Toast.LENGTH_LONG).show();
            return;
        }

        if (!oldPassword.equals(AuthInformation.getInstance().getUserCredentials().password)) {
            Toast.makeText(this, "Old password is wrong", Toast.LENGTH_LONG).show();
            return;
        }

        if (newPassword.length() < 1) {
            Toast.makeText(this, "Please fill in the new password.", Toast.LENGTH_LONG).show();
            return;
        }

        if (confirmNewPassword.length() < 1) {
            Toast.makeText(this, "Please confirm the new password.", Toast.LENGTH_LONG).show();
            return;
        }

        if (!newPassword.equals(confirmNewPassword)) {
            Toast.makeText(this, "New password confirmation is wrong", Toast.LENGTH_LONG).show();
            return;
        }

        if (oldPassword.equals(newPassword)) {
            Toast.makeText(this, "The new password is the same as old password.", Toast.LENGTH_LONG).show();
            return;
        }

        // create user credentials
        UserCredentials userCredentials = new UserCredentials();
        userCredentials.username = txtUsername.getText().toString();
        userCredentials.password = oldPassword;

        // send changes to server
        if (attemptPasswordUpdate == null || attemptPasswordUpdate.getStatus() != AsyncTask.Status.RUNNING) {
            attemptPasswordUpdate = new AttemptPasswordUpdate(userCredentials, newPassword);
            attemptPasswordUpdate.execute();
        } else {
            Toast.makeText(this, "Please wait.", Toast.LENGTH_SHORT).show();
        }
    }

    public class AttemptPasswordUpdate extends AsyncTask<Void, Void, Boolean> {

        private final UserCredentials userCredentials;
        private final String newPassword;
        private Socket socket = null;

        public AttemptPasswordUpdate(UserCredentials userCredentials, String newPassword) {
            this.userCredentials = userCredentials;
            this.newPassword = newPassword;
        }

        @Override
        public Boolean doInBackground(Void... voids) {
            try {
                socket = new Socket();
                socket.connect(new InetSocketAddress(AppConfig.HOST, 3010), 3000);

                if (socket.isConnected())
                    socket.setSoTimeout(5000);
                else
                    return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            ServerConnection serverConnection = new ServerConnection(socket);
            boolean updateSuccessful = false;

            UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest();
            updatePasswordRequest.userCredentials = userCredentials;
            updatePasswordRequest.newPassword = newPassword;

            if (serverConnection.writeObject(updatePasswordRequest)) {
                {
                    UpdatePasswordResponse updatePasswordResponse = (UpdatePasswordResponse) (serverConnection.readObject());
                    if (updatePasswordResponse != null) {
                        updateSuccessful = updatePasswordResponse.status;
                    }
                }
            }

            serverConnection.closeConnection();
            return updateSuccessful;
        }

        @Override
        public void onPostExecute(Boolean loginSucceeded) {
            if (loginSucceeded) {
                // update password locally as well
                userCredentials.password = newPassword;
                AuthInformation.createAuthInformation(userCredentials);

                onSuccessfullyPasswordUpdate();
            } else if (socket.isConnected()) {
                Toast.makeText(getApplicationContext(), "Login credentials are wrong", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "Failed to connect to server.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void onSuccessfullyPasswordUpdate() {
        Toast.makeText(this, "Password updated succesfully.", Toast.LENGTH_LONG).show();
    }


    public void onSignOut(View view) {
    }
}
