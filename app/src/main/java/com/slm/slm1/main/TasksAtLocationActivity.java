package com.slm.slm1.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.model.LatLng;
import com.slm.slm1.R;
import com.slm.slm1.dao.LocationDao;
import com.slm.slm1.dao.TaskDao;
import com.slm.slm1.dao.TaskDetailsDao;
import com.slm.slm1.helper.AppConfig;
import com.slm.slm1.helper.AppDatabase;
import com.slm.slm1.helper.DatabaseUtil;
import com.slm.slm1.helper.ServerConnection;
import com.slm.slm1.helper.TaskSerializable;
import com.slm.slm1.helper.TasksToDeleteManager;
import com.slm.slm1.helper.Utils;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.TaskAndTaskDetails;

public class TasksAtLocationActivity extends AppCompatActivity {

    private LinearLayout deleteActionsLayout;
    private LinearLayout addActionLayout;
    private ToggleButton tglTasksList;
    private ListView lstToDoTasks;

    private LatLng mLocation;
    private Location mLocationEntity = null;

    private List<TaskAndTaskDetails> mToDoTasks;

    private boolean mCompletedOrNot = false;
    private boolean deleteMode = false;
    private List<TaskAndTaskDetails> tasksToDelete;

    private DeleteTasksTask deleteTasksTask;

    OnItemClickListener lstListener = new OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view,
                                int position, long id) {
            TaskAndTaskDetails current = (TaskAndTaskDetails) parent.getItemAtPosition(position);

            if (deleteMode && !tasksToDelete.contains(current)) {
                tasksToDelete.add(current);
                TextView textView = (TextView) view;
                textView.setTextColor(getResources().getColor(R.color.textToDelete));

            } else if (deleteMode) {
                tasksToDelete.remove(current);
                TextView textView = (TextView) view;
                textView.setTextColor(getResources().getColor(android.R.color.background_dark));
            } else {
                Intent intent = new Intent(getApplicationContext(), TaskFormActivity.class);
                intent.putExtra("taskid", current.task.id);

                startActivity(intent);

            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks_at_location);

        Intent intent = getIntent();
        mLocation = intent.getParcelableExtra("location");



        deleteActionsLayout = findViewById(R.id.deleteActionsLayout);
        addActionLayout = findViewById(R.id.addActionLayout);
        tglTasksList = findViewById(R.id.tglTasksList);
        lstToDoTasks = findViewById(R.id.lstToDoTasks);

        tglTasksList.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                mCompletedOrNot = isChecked;
                updateListing();
            }
        });
        lstToDoTasks.setOnItemClickListener(lstListener);

        updateListing();
    }

    @Override
    public void onNewIntent(Intent newIntent) {
        // use the new intent not the original one
        super.onNewIntent(newIntent);
        setIntent(newIntent);
    }

    @Override
    protected void onStart() {
        super.onStart();

        LocationDao locationDao = AppDatabase.getInstance().locationDao();
        LatLng range = Utils.distanceToCoordinates(0.2);
        List<Location> candidateLocations = locationDao.findLocations(mLocation.latitude,
                mLocation.longitude, range.latitude, range.longitude);

        if (candidateLocations.size() == 1)
            Log.d("Location", "Location don't overlap");

        double shortestDistanceSoFar = Double.MAX_VALUE;
        for (Location location : candidateLocations) {
            double distanceToLocation = Math.hypot( Math.abs(mLocation.latitude - location.lat),
                    Math.abs(mLocation.longitude - location.lg));

            if (distanceToLocation < shortestDistanceSoFar) {
                shortestDistanceSoFar = distanceToLocation;
                mLocationEntity = location;
            }
        }

        if (mLocationEntity == null && candidateLocations.size() > 0)
            throw new AssertionError();

        if (mLocationEntity != null)
            mLocation = new LatLng(mLocationEntity.lat, mLocationEntity.lg);

        updateListing();
        deleteActionsLayout.setVisibility(View.INVISIBLE);
    }

    public void updateListing() {
        if (mLocationEntity != null) {
            TaskDao taskDao = AppDatabase.getInstance().taskDao();
            mToDoTasks = taskDao.getTaskAndTaskDetailsOnLocation(mLocationEntity.id, mCompletedOrNot ? 1 : 0);

            ArrayAdapter<TaskAndTaskDetails> todoTasksArrayAdapter =
                    new ArrayAdapter<TaskAndTaskDetails>(this, android.R.layout.simple_list_item_1, mToDoTasks);
            lstToDoTasks.setAdapter(todoTasksArrayAdapter);

            if (mToDoTasks.isEmpty()) {
                List<TaskAndTaskDetails> otherTaskList;
                otherTaskList = taskDao.getTaskAndTaskDetailsOnLocation(mLocationEntity.id, mCompletedOrNot ? 0 : 1);
                if (otherTaskList.isEmpty()) {
                    // remove location from database, to disable notification
                    LocationDao locationDao = AppDatabase.getInstance().locationDao();
                    locationDao.delete(mLocationEntity);
                }
            }
        }
    }

    public void onDelete(View view) {
        deleteMode = true;
        tasksToDelete = new ArrayList<>();
        deleteActionsLayout.setVisibility(View.VISIBLE);
        addActionLayout.setVisibility(View.INVISIBLE);
    }

    public void onConfirmDeletion(View view) {

        TaskDao taskDao = AppDatabase.getInstance().taskDao();
        TaskDetailsDao taskDetailsDao = AppDatabase.getInstance().taskDetailsDao();

        if (tasksToDelete != null && !tasksToDelete.isEmpty()) {
            for (TaskAndTaskDetails taskAndTaskDetails : tasksToDelete) {
                taskDao.deleteTasks(taskAndTaskDetails.task);
                taskDetailsDao.deleteTaskDetails(taskAndTaskDetails.taskDetails);
            }
            TasksToDeleteManager.getInstance().addTasksToDelete(DatabaseUtil.convertToSerializable(tasksToDelete));

            tasksToDelete.clear();
            updateListing();

            if (deleteTasksTask == null || deleteTasksTask.getStatus() != AsyncTask.Status.RUNNING) {
                deleteTasksTask = new DeleteTasksTask();
                deleteTasksTask.execute();
            } else {
                throw new RuntimeException("Delete task is already running");
            }
        }

        deleteActionsLayout.setVisibility(View.INVISIBLE);
        addActionLayout.setVisibility(View.VISIBLE);
        deleteMode = false;
        tasksToDelete = null;

        MapActivity.notifyForNotificationUpdate();
    }

    public void onDiscardDeletion(View view) {
        tasksToDelete.clear();
        tasksToDelete = null;
        deleteMode = false;
        updateListing();
        deleteActionsLayout.setVisibility(View.INVISIBLE);
        addActionLayout.setVisibility(View.VISIBLE);
    }

    public void onAddTask(View view) {
        Intent intent = new Intent(this, TaskFormActivity.class);
        intent.putExtra("taskid", -1);
        intent.putExtra("location", mLocation); // TODO -- update to nearst location --> Done updated when the nearest location is set
        startActivity(intent);
    }

    public class DeleteTasksTask extends AsyncTask<Void, Void, Boolean> {

        private List<TaskSerializable> tasksToDelete;

        @Override
        protected Boolean doInBackground(Void... voids) {
            tasksToDelete = TasksToDeleteManager.getInstance().getTasksToDelete();
            Boolean success = false;

            if (tasksToDelete != null && !tasksToDelete.isEmpty()) {
                Socket socket;
                try {
                    socket = new Socket(AppConfig.HOST, 3008);
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                }

                ServerConnection serverConnection = new ServerConnection(socket);

                if (serverConnection.writeObject(tasksToDelete)) {
                    Boolean response = (Boolean) serverConnection.readObject();
                    if (response != null) {
                        success = response;
                    }
                }

                serverConnection.closeConnection();
            } else {
                success = true;
            }

            return success;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (!success) {
                TasksToDeleteManager.getInstance().addTasksToDelete(tasksToDelete);
            }
        }
    }
}
