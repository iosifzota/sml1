package com.slm.slm1.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.slm.slm1.R;
import com.slm.slm1.dao.LocationDao;
import com.slm.slm1.dao.TaskDao;
import com.slm.slm1.dao.TaskDetailsDao;
import com.slm.slm1.helper.AppConfig;
import com.slm.slm1.helper.AppDatabase;
import com.slm.slm1.helper.AuthInformation;
import com.slm.slm1.helper.DeviceLocationCoords;
import com.slm.slm1.helper.Observer;
import com.slm.slm1.helper.RestoreRequest;
import com.slm.slm1.helper.ServerConnection;
import com.slm.slm1.helper.TaskSerializable;
import com.slm.slm1.helper.TasksAtLocationNotificationManager;
import com.slm.slm1.helper.UpdateNotificationListener;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskDetails;


import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;
import java.net.Socket;
import java.util.List;

/**
 *
 * TODO:     java.lang.RuntimeException: Unable to start activity ComponentInfo{com.slm.slm1/com.slm.slm1.main.MapActivity}:
 * android.view.InflateException: Binary XML file line #10: Binary XML file line #10: Error inflating class fragment
 *  - XML cu cheie :)
 */

public class MapActivity extends AppCompatActivity
        implements OnMapReadyCallback, GoogleMap.OnMapLongClickListener, LocationListener {

    private GoogleMap mMap;
    private static final LatLng romania = new LatLng(45.65317,25.6077933);

    private static boolean setupDone = false;
    private static boolean restoreDone = false;
    public static Location deviceLocation = null;
    private LatLng deviceCoords;
    private boolean deviceLocationUp;
    private static Observer notificationUpdateObserver = new Observer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_activity);

//        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
//        setSupportActionBar(myToolbar);

        SupportMapFragment mapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        AppDatabase.setContext(this);
        setupDone = false;
        restoreDone = false;
        if (savedInstanceState != null) {
            setupDone = savedInstanceState.getBoolean("setupDone");
        }

        notificationUpdateObserver.addListener(new UpdateNotificationListener(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in ROmania and move the camera
        mMap.addMarker(new MarkerOptions().position(romania).title("Brasov, Romania"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(romania));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(mMap.getMaxZoomLevel() - 10));

        // Enable zooming
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);

        // Listen for click events
        mMap.setOnMapLongClickListener(this);
        updateListing();
    }

    @Override
    public void onMapLongClick(LatLng point) {
        updateMarker(new MarkerOptions().position(point).title("Mark"));

        Double lat = ((double)((int)(point.latitude * 10000)))/10000;
        Double lg = ((double)((int)(point.longitude * 10000)))/10000;
        LatLng location = new LatLng(lat, lg);

        Intent intent = new Intent(this, TasksAtLocationActivity.class);
        intent.putExtra("location", location);

        startActivity(intent);
    }

    public void updateMarker(MarkerOptions markerOptions) {
        mMap.clear();
        mMap.addMarker(markerOptions);
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateListing();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!setupDone) {
            setupDone = true;
            Intent intent = new Intent(this, SetupActivity.class);
            startActivity(intent);
        } else if (!restoreDone) {
            RestoreTasksTask restoreTasksTask = new RestoreTasksTask();
            restoreTasksTask.execute();
        } else {
            updateListing();
        }

        if (!deviceLocationUp && hasLocationPermission()) {
            requestLocationUpdates();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putBoolean("setupDone", setupDone);
        super.onSaveInstanceState(outState);
    }

    private void updateListing() {
        if (mMap == null) {
            return;
        }

        mMap.clear();
        if (deviceLocationUp) {
            LatLng deviceLocationCoords =  DeviceLocationCoords.getInstance().getLocation();

            if (deviceLocationCoords != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(deviceLocationCoords).title("Current position").alpha(0.5f);
                mMap.addMarker(markerOptions);
            }
        }

        List<Location> locations = AppDatabase.getInstance().locationDao().getAll();

        for (Location location : locations) {
            LatLng latLng = new LatLng(location.lat, location.lg);
            mMap.addMarker(new MarkerOptions().position(latLng));
        }
    }

    public void onShowTasksAtLocation(View view) {
        LatLng currentLocation = DeviceLocationCoords.getInstance().getLocation();

        if (currentLocation != null) {
            Intent intent = new Intent(this, TasksAtLocationActivity.class);
            intent.putExtra("location", currentLocation);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Current location not yet available.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        // aproximate
        double myLatitude = (int) (location.getLatitude() * 10000) / 10000.0d;
        double myLongitude = (int) (location.getLongitude() * 10000) / 10000.0d;

        LatLng tmpCoords = new LatLng(myLatitude, myLongitude);
        if (deviceCoords == null || !deviceCoords.equals(tmpCoords)) {
            deviceCoords = tmpCoords;
            DeviceLocationCoords.getInstance().setLocation(tmpCoords);
            updateListing();
        }

        updateTasksNotification(this);
    }

    public static void updateTasksNotification(Context applicationContext) {
        updateTasksNotification(applicationContext, false);
    }

    public static void updateTasksNotification(Context applicationContext, boolean forceUpdate) {
        LocationDao locationDao = AppDatabase.getInstance().locationDao();

        LatLng currentLocationCoords = DeviceLocationCoords.getInstance().getLocation();
        com.slm.slm1.model.Location currentLocation = null;

        if (currentLocationCoords != null)
            currentLocation = locationDao.find(currentLocationCoords.latitude, currentLocationCoords.longitude);

        if (currentLocation != null && (!currentLocation.equals(MapActivity.deviceLocation) || forceUpdate)) {
            TasksAtLocationNotificationManager notificationManager = TasksAtLocationNotificationManager.getInstance();
            notificationManager.buildNotification(applicationContext, currentLocation);
        } else if (currentLocation == null && MapActivity.deviceLocation != null) {
            // disable notification for empty location
            NotificationManager notificationManager = (NotificationManager) applicationContext.getSystemService(NOTIFICATION_SERVICE);
            notificationManager.cancel(TasksAtLocationNotificationManager.NOTIFCATION_ID);
        }

        MapActivity.deviceLocation = currentLocation;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    void requestLocationUpdates() {
        LocationManager locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            deviceLocationUp = false;
            return;
        }

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
        deviceLocationUp = true;
    }

    boolean hasLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }

    public void onSearchTasks(View view) {
        Intent intent = new Intent(this, SearchTasksActivity.class);
        startActivity(intent);
    }

    public class RestoreTasksTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {

            if (!AppDatabase.getInstance().taskDao().getAll().isEmpty()) {
                AppDatabase.getInstance().taskDao().nukeTable();
                AppDatabase.getInstance().taskDetailsDao().nukeTable();
                AppDatabase.getInstance().locationDao().nukeTable();
//                return true;
            }

            Socket socket;
            try {
                socket = new Socket(AppConfig.HOST, 3007);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            ServerConnection serverConnection = new ServerConnection(socket);

            RestoreRequest restoreRequest = new RestoreRequest();
            restoreRequest.username = AuthInformation.getInstance().getUserCredentials().username;
            boolean success = false;

            if (serverConnection.writeObject(restoreRequest)) {
                List<TaskSerializable> tasks = (List<TaskSerializable>) serverConnection.readObject();

                if (tasks != null && !tasks.isEmpty()) {
                    TaskDao taskDao = AppDatabase.getInstance().taskDao();
                    TaskDetailsDao taskDetailsDao = AppDatabase.getInstance().taskDetailsDao();
                    LocationDao locationDao = AppDatabase.getInstance().locationDao();

                    for (TaskSerializable taskSerializable : tasks) {
                        Location location = locationDao.find(taskSerializable.locationLat, taskSerializable.locationLg);

                        if (location == null) {
                            Location tmpLocation = new Location(taskSerializable.locationLat, taskSerializable.locationLg);
                            locationDao.insert(tmpLocation);

                            location = locationDao.find(taskSerializable.locationLat, taskSerializable.locationLg);
                        }

                        Task task = new Task();
                        task.name = taskSerializable.taskName;
                        task.completionStatus = taskSerializable.completionStatus;
                        task.deadline = taskSerializable.deadline;
                        task.serverTaskId = taskSerializable.localTaskId;
                        task.locationfk = location.id;

                        long rowId = taskDao.insertTask(task);

                        TaskDetails taskDetails = new TaskDetails();
                        taskDetails.notes = taskSerializable.taskNotes;
                        taskDetails.taskfk = (int)rowId;

                        taskDetailsDao.insertTaskDetails(taskDetails);
                        Log.d("msg", taskSerializable.taskName + " - saved");
                    }

                    success = true;
                }
            }

            serverConnection.closeConnection();
            return success;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            if (!success) {
                Log.d("msg", "Failed restore from database");
            }
            restoreDone = true;
            updateListing();
        }
    }

    public static void notifyForNotificationUpdate() {
        notificationUpdateObserver.notifyChange();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_user_settings:
                Intent intent = new Intent(this, UserSettingsActivity.class);
                startActivity(intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }
}
