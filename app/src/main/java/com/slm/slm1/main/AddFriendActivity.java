package com.slm.slm1.main;

import androidx.appcompat.app.AppCompatActivity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.slm.slm1.R;
import com.slm.slm1.dao.FriendDao;
import com.slm.slm1.helper.AddFriendRequest;
import com.slm.slm1.helper.AddFriendResponse;
import com.slm.slm1.helper.AppConfig;
import com.slm.slm1.helper.AppDatabase;
import com.slm.slm1.helper.AuthInformation;
import com.slm.slm1.helper.ServerConnection;
import com.slm.slm1.model.Friend;

import java.io.IOException;
import java.net.Socket;

public class AddFriendActivity extends AppCompatActivity {

    EditText edFriendName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friend);

        edFriendName = findViewById(R.id.edFriendName);
    }

    public void onAddFriend(View view) {
        String friendUsername = edFriendName.getText().toString();
        AddFriendRequest addFriendRequest = new AddFriendRequest();
        addFriendRequest.senderUsername = AuthInformation.getInstance().getUserCredentials().username;
        addFriendRequest.targetUsername = friendUsername;

        AddFriendRunner addFriendRunner = new AddFriendRunner(addFriendRequest);
        addFriendRunner.execute();
    }

    public class AddFriendRunner extends AsyncTask<Void, Void, Boolean> {

        private final AddFriendRequest addFriendRequest;

        public AddFriendRunner(AddFriendRequest addFriendRequest) {
            this.addFriendRequest = addFriendRequest;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            Socket socket;
            try {
                socket = new Socket(AppConfig.HOST, 3003);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            ServerConnection serverConnection = new ServerConnection(socket);
            boolean success = false;

            if (serverConnection.writeObject(addFriendRequest)) {
                // request send

                AddFriendResponse addFriendResponse = (AddFriendResponse) serverConnection.readObject();

                if (addFriendResponse != null) {
                    success = addFriendResponse.success;
                }
            }

            serverConnection.closeConnection();
            return success;
        }

        @Override
        public void onPostExecute(Boolean success) {
            if (success) {
                // save friend in the local database
                Friend friend = new Friend();
                friend.username = addFriendRequest.targetUsername;

                FriendDao friendDao = AppDatabase.getInstance().friendDao();
                friendDao.insert(friend);
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Username does not exist.", Toast.LENGTH_LONG).show();
            }
        }
    }
}
