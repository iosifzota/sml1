package com.slm.slm1.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.slm.slm1.R;
import com.slm.slm1.dao.FriendDao;
import com.slm.slm1.dao.LocationDao;
import com.slm.slm1.dao.TaskDao;
import com.slm.slm1.helper.AppConfig;
import com.slm.slm1.helper.AppDatabase;
import com.slm.slm1.helper.AuthInformation;
import com.slm.slm1.helper.FriendSerializable;
import com.slm.slm1.helper.FriendsListRequest;
import com.slm.slm1.helper.SendTaskRunner;
import com.slm.slm1.helper.ServerConnection;
import com.slm.slm1.helper.TaskSerializable;
import com.slm.slm1.model.Friend;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.TaskAndTaskDetails;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class SendTaskActivity extends AppCompatActivity {

    private TaskAndTaskDetails mTask;

    private EditText edNameToSearchFor;
    private ListView lstFriends;

    public static OutputStream outputStream;
    public static ObjectOutputStream objectOutputStream;

    private GetFriendsListTask getFriendsListTask;

    private boolean isSend = false;

    private AdapterView.OnItemClickListener lstFriendsListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            // send to friend
            TextView textView = (TextView) view;
            String friendUsername = textView.getText().toString();

            // send task
            final TaskSerializable taskSerializable = new TaskSerializable();
            taskSerializable.taskName = mTask.task.name;
            taskSerializable.taskNotes = mTask.taskDetails.notes;

            LocationDao locationDao = AppDatabase.getInstance().locationDao();
            Location location = locationDao.getLocation(mTask.task.locationfk);
            taskSerializable.locationLat = location.lat;
            taskSerializable.locationLg = location.lg;
            taskSerializable.completionStatus = mTask.task.completionStatus;
            taskSerializable.targetUsername = friendUsername;
            taskSerializable.deadline = mTask.task.deadline;
            taskSerializable.localTaskId = mTask.task.serverTaskId;

            if (mTask.task.serverTaskId == -1) {
                throw new RuntimeException("This task is not saved on the server");
            }

            SendTaskTask thread = new SendTaskTask(taskSerializable);
            thread.execute();
        }
    };

    public class SendTaskTask extends AsyncTask<Void, Void, Boolean> {


        private final TaskSerializable taskSerializable;

        public SendTaskTask(TaskSerializable taskSerializable) {
            this.taskSerializable = taskSerializable;
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            Socket socket;
            try {
                socket = new Socket(AppConfig.HOST, 3001);
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            ServerConnection serverConnection = new ServerConnection(socket);
            Boolean success = false;

            if (serverConnection.writeObject(taskSerializable)) {
                Boolean response = (Boolean) serverConnection.readObject();
                if (response != null) {
                    success = response;
                }
            }
            serverConnection.closeConnection();
            return success;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            isSend = success;
            Intent resultIntent = new Intent();
            resultIntent.putExtra("isSend", isSend);
            setResult(0, resultIntent);
            finish();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_to_friend);

        Intent intent = getIntent();
        int taskid = intent.getIntExtra("taskid", -1);

        TaskDao taskDao = AppDatabase.getInstance().taskDao();
        mTask = taskDao.getTaskAndTaskDetails(taskid);

        edNameToSearchFor = findViewById(R.id.edNameToSearchFor);
        lstFriends = findViewById(R.id.lstFriends);

        lstFriends.setOnItemClickListener(lstFriendsListener);
    }

    @Override
    protected void onStart() {
        super.onStart();

        updateListing();
    }

    boolean oneTime = false;

    private void updateListing() {
        FriendDao friendDao = AppDatabase.getInstance().friendDao();

        if (!oneTime) {
            friendDao.nukeDatabase();
            oneTime = true;
        }

        List<Friend> friends = friendDao.getAllFriends();

        if ((friends == null || friends.isEmpty()) && getFriendsListTask == null) {
            getFriendsListTask = new GetFriendsListTask();
            getFriendsListTask.execute();
        } else if (friends != null) {
            ArrayAdapter<Friend> friendsArrayAdapater = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, friends);
            lstFriends.setAdapter(friendsArrayAdapater);
        }
    }

    public void onSearchForFriend(View view) {
        // to do
    }

    public void onAddANewFriend(View view) {
        Intent intent = new Intent(this, AddFriendActivity.class);
        startActivity(intent);
    }

    public class GetFriendsListTask extends AsyncTask<Void, Void, List<FriendSerializable>> {
        @Override
        public List<FriendSerializable> doInBackground(Void... voids) {
            Socket socket;
            try {
                socket = new Socket(AppConfig.HOST, 3005);
            } catch (IOException e) {
                e.printStackTrace();
                return new ArrayList<>();
            }

            ServerConnection serverConnection = new ServerConnection(socket);

            List<FriendSerializable> friends = null;

            FriendsListRequest friendsListRequest = new FriendsListRequest();
            friendsListRequest.senderUsername = AuthInformation.getInstance().getUserCredentials().username;

            if (serverConnection.writeObject(friendsListRequest)) {
                friends = (List<FriendSerializable>) serverConnection.readObject();
            }

            if (friends == null) {
                friends = new ArrayList<>();
            }

            serverConnection.closeConnection();
            return friends;
        }

        @Override
        public void onPostExecute(List<FriendSerializable> friends) {
            if (!friends.isEmpty()) {
                // add friends to the local database
                FriendDao friendDao = AppDatabase.getInstance().friendDao();

                for (FriendSerializable friendSerializable : friends) {
                    Friend friend = new Friend();
                    friend.username = friendSerializable.username;

                    friendDao.insert(friend);
                }

                updateListing();
            }
        }
    }
}
