package com.slm.slm1.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.slm.slm1.model.Record;
import com.slm.slm1.model.RecordWithValues;

import java.util.List;

@Dao
public interface RecordDao {
    @Transaction
    @Query("SELECT * FROM Record")
    List<RecordWithValues> getRecordsWithValues();

    @Query("SELECT * FROM Record")
    List<Record> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long insertRecord(Record record);


    @Query("DELETE FROM Record")
    public void nukeTable();
}
