package com.slm.slm1.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.slm.slm1.model.User;

@Dao
public interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long insert(User user);

    @Query("SELECT * FROM User WHERE username = :username")
    public User getUser(String username);

    @Query("SELECT * FROM User")
    public User getUser();

    @Update
    public void update(User... users);
}
