package com.slm.slm1.dao;

import androidx.room.Dao;
import androidx.room.Query;

@Dao
public interface LocationDetailsDao {
    @Query("DELETE FROM LocationDetails")
    public void nukeTable();
}
