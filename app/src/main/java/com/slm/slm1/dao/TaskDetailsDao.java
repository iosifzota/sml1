package com.slm.slm1.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskDetails;

@Dao
public interface TaskDetailsDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertTaskDetails(TaskDetails... taskDetails);

    @Update
    public void updateTasks(TaskDetails... details);

    @Query("DELETE FROM TaskDetails")
    public void nukeTable();

    @Delete
    public void deleteTaskDetails(TaskDetails... tasks);

    @Update
    void updateTasks(Task... tasks);
}
