package com.slm.slm1.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.slm.slm1.model.Friend;

import java.util.List;

@Dao
public interface FriendDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long insert(Friend friend);

    @Query("SELECT * FROM Friend")
    public List<Friend> getAllFriends();

    @Query("SELECT * FROM Friend WHERE id = :friendid")
    public Friend getFriend(int friendid);

    @Query("SELECT * FROM Friend WHERE username = :friendUsername")
    public Friend getFriend(String friendUsername);

    @Query("DELETE FROM Friend")
    public void nukeDatabase();
}
