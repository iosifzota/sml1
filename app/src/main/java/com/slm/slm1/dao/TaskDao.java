package com.slm.slm1.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;
import androidx.room.Update;

import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskAndTaskDetails;
import com.slm.slm1.model.TaskWithRecords;
import com.slm.slm1.model.TaskWithRecordsAndValues;

import java.util.Date;
import java.util.List;

@Dao
public interface TaskDao {
    @Transaction
    @Query("SELECT * FROM Task")
    public List<TaskAndTaskDetails> getTasksAndTaskDetails();

    @Transaction
    @Query("SELECT * FROM Task WHERE Task.locationfk = :locationId AND Task.completion_status = :completedOrNot")
    public List<TaskAndTaskDetails> getTaskAndTaskDetailsOnLocation(int locationId, int completedOrNot);

    @Transaction
    @Query("SELECT * FROM Task WHERE Task.id = :taskid")
    public TaskAndTaskDetails getTaskAndTaskDetails(int taskid);

    @Transaction
    @Query("SELECT * FROM Task")
    public List<TaskWithRecords> TasksWithRecords();

    @Transaction
    @Query("SELECT * FROM Task")
    public List<TaskWithRecordsAndValues> TasksWithRecordsAndValues();

    @Transaction
    @Query("SELECT * FROM Task WHERE Task.id = :taskid")
    public List<TaskWithRecordsAndValues> TasksWithRecordsAndValues(int taskid);

    @Query("SELECT * FROM Task")
    List<Task> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insertTasks(Task... tasks);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long insertTask(Task task);


    @Query("SELECT * FROM Task WHERE Task.id = :taskid")
    public Task getTask(int taskid);

    @Query("DELETE FROM Task")
    public void nukeTable();

    @Delete
    public void deleteTasks(Task... tasks);

    @Transaction
    @Query("SELECT * FROM Task WHERE Task.name LIKE :taskName")
    public List<TaskAndTaskDetails> getTasks(String taskName);

    @Update
    public void update(Task... tasks);

    @Transaction
    @Query("SELECT * FROM Task WHERE Task.locationfk = :locationId AND Task.completion_status = 0 AND Task.deadline = :deadline")
    public List<TaskAndTaskDetails> getTaskAndTaskDetailsOnLocationOnDate(int locationId, Date deadline);
}
