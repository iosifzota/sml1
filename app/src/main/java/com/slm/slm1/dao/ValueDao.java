package com.slm.slm1.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.slm.slm1.model.Value;

@Dao
public interface ValueDao {
    @Query("DELETE FROM Value")
    public void nukeTable();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public void insert(Value value);
}
