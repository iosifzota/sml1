package com.slm.slm1.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.slm.slm1.model.LoggedUser;

@Dao
public interface LoggedUserDao {

    @Insert
    public long insert(LoggedUser loggedUser);

    @Query("SELECT * FROM LoggedUser")
    public LoggedUser getLoggedUser();
}
