package com.slm.slm1.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.slm.slm1.model.Location;
import com.slm.slm1.model.LocationAndLocationDetails;

import java.util.List;

@Dao
public interface LocationDao {
    @Transaction
    @Query("SELECT * FROM Location")
    List<LocationAndLocationDetails> getLocationsAndLocationDetails();

    @Query("SELECT * FROM Location")
    List<Location> getAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertLocation(Location... locations);

    @Query("DELETE FROM Location")
    public void nukeTable();

    @Query("SELECT * FROM Location WHERE lat = :lat AND lg = :lg")
    public Location find(double lat, double lg);

    @Query("SELECT * FROM Location WHERE lat >= :lat - :rangeLat AND lat <= :lat + :rangeLat AND lg >= :lg - :rangeLg AND lg <= :lg + :rangeLg")
    public List<Location> findLocations(double lat, double lg, double rangeLat, double rangeLg);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    public long insert(Location location);

    @Query("SELECT * FROM Location WHERE id = :location")
    public Location getLocation(int location);

    @Delete
    void delete(Location... locations);
}
