package com.slm.slm1.helper;

import java.io.Serializable;
import java.util.Date;

public class TaskSerializable implements Serializable {
    public int localTaskId;
    public String taskName;
    public String taskNotes;
    public double locationLat;
    public double locationLg;
    public boolean completionStatus;
    public String targetUsername;
    public Date deadline;
}
