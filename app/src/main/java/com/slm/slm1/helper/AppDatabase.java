package com.slm.slm1.helper;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;

import com.slm.slm1.dao.FriendDao;
import com.slm.slm1.dao.LocationDao;
import com.slm.slm1.dao.LocationDetailsDao;
import com.slm.slm1.dao.LoggedUserDao;
import com.slm.slm1.dao.RecordDao;
import com.slm.slm1.dao.TaskDao;
import com.slm.slm1.dao.TaskDetailsDao;
import com.slm.slm1.dao.UserDao;
import com.slm.slm1.dao.ValueDao;
import com.slm.slm1.model.Converters;
import com.slm.slm1.model.Friend;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.LocationDetails;
import com.slm.slm1.model.LoggedUser;
import com.slm.slm1.model.Record;
import com.slm.slm1.model.Task;
import com.slm.slm1.model.TaskDetails;
import com.slm.slm1.model.User;
import com.slm.slm1.model.Value;

@Database(entities = {
        Task.class,
        TaskDetails.class,
        Location.class,
        LocationDetails.class,
        Record.class,
        Value.class,
        Friend.class,
        User.class,
        LoggedUser.class
}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase instance = null;

    public static Context getContext() {
        return mContext;
    }

    public static void setContext(Context ctx) {
        AppDatabase.mContext = ctx;
    }

    private static Context mContext = null;

    public static AppDatabase getInstance() {
        if (null == instance) {
            instance = Room.databaseBuilder(mContext, AppDatabase.class,
                    "task.db").allowMainThreadQueries().build();
        }
        return instance;
    }

    public abstract TaskDao taskDao();
    public abstract TaskDetailsDao taskDetailsDao();
    public abstract LocationDao locationDao();
    public abstract LocationDetailsDao locationDetailsDao();
    public abstract RecordDao recordDao();
    public abstract ValueDao valueDao();
    public abstract FriendDao friendDao();
    public abstract UserDao userDao();
    public abstract LoggedUserDao loggedUserDao();
}
