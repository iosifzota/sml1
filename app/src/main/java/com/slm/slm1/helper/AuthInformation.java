package com.slm.slm1.helper;

public class AuthInformation {

    private static AuthInformation instance;

    private UserCredentials userCredentials;

    public AuthInformation(UserCredentials userCredentials) {
        this.userCredentials = userCredentials;
    }

    public static AuthInformation getInstance() {
        if (instance == null) {
            throw new RuntimeException("create AuthInformation should be called first.");
        }
        return instance;
    }

    public static AuthInformation createAuthInformation(UserCredentials userCredentials) {
        if (userCredentials == null)
            throw new AssertionError("Null input");
        if (instance == null)
            instance = new AuthInformation(userCredentials);
        else
            getInstance().setUserCredentials(userCredentials);
        return instance;
    }

    public UserCredentials getUserCredentials() {
        return userCredentials;
    }

    public void setUserCredentials(UserCredentials userCredentials) {
        this.userCredentials = userCredentials;
    }
}
