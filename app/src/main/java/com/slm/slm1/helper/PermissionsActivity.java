package com.slm.slm1.helper;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.Toast;

import com.slm.slm1.R;

public class PermissionsActivity extends AppCompatActivity {

    final static int PERM_REQ_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permissions);

        // Check whether GPS tracking is enabled
        LocationManager lm = (LocationManager) getSystemService(LOCATION_SERVICE);
        if(!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            finish();
        }

        // Check whether this app has access to location permission
        final int perm = ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);

        // Check whether this app has access to foreground service
        final int permFService = ContextCompat.checkSelfPermission(this,
                Manifest.permission.FOREGROUND_SERVICE);

        // If location perm has been granted, then start the Tracker Service
        if (perm == PackageManager.PERMISSION_GRANTED
                && permFService == PackageManager.PERMISSION_GRANTED) {
            finish();
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.FOREGROUND_SERVICE},
                    PERM_REQ_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // If perm granted
        if (requestCode == PERM_REQ_CODE //&& grantResults.length == 1
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) { //[1] ??
            // start the GPS tracking service
            finish();
        } else {
            // Display a toast with more info
            Toast.makeText(this, "Please enable location services to allow GPS traking", Toast.LENGTH_SHORT).show();
        }
    }
}
