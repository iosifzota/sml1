package com.slm.slm1.helper;

import com.google.android.gms.maps.model.LatLng;

import java.util.concurrent.locks.ReentrantLock;

public class DeviceLocationCoords {

    private LatLng location;
    private ReentrantLock lock;

    private static DeviceLocationCoords instance;

    public DeviceLocationCoords() {
        this.lock = new ReentrantLock();
    }

    public static DeviceLocationCoords getInstance() {
        if (instance == null) {
            instance = new DeviceLocationCoords();
        }
        return instance;
    }

    public LatLng getLocation() {
        lock.lock();
        LatLng output = location;
        lock.unlock();
        return output;
    }

    public void setLocation(LatLng location) {
        lock.lock();
        this.location = location;
        lock.unlock();
    }
}
