package com.slm.slm1.helper;

import java.util.ArrayList;
import java.util.List;

public class Observer {

    private List<Listener> listeners = new ArrayList<>();

    public Observer() {
    }

    public Observer(Listener listener) {
        listeners.add(listener);
    }

    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public void notifyChange() {
        for (Listener listener : listeners) {
            listener.update();
        }
    }
}
