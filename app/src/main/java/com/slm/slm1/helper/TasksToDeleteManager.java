package com.slm.slm1.helper;

import com.slm.slm1.model.TaskAndTaskDetails;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TasksToDeleteManager {

    private static TasksToDeleteManager instance;
    private List<TaskSerializable> tasksToDelete;
    private Lock lock;

    public TasksToDeleteManager() {
        tasksToDelete = new ArrayList<>();
        lock = new ReentrantLock();
    }

    public static TasksToDeleteManager getInstance() {
        if (instance == null) {
            instance = new TasksToDeleteManager();
        }
        return instance;
    }

    public void addTasksToDelete(List<TaskSerializable> list) {
        lock.lock();
        tasksToDelete.addAll(list);
        lock.unlock();
    }

    public List<TaskSerializable> getTasksToDelete() {
        lock.lock();
        List<TaskSerializable> list = tasksToDelete;
        tasksToDelete = new ArrayList<>();
        lock.unlock();
        return list;
    }

}
