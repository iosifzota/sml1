package com.slm.slm1.helper;

import android.content.Context;

public class TaskReceivedNotificationManager {

    public static final int MAX_SLOTS = 4;
    private int slotCounter;

    private static TaskReceivedNotificationManager instance;

    private TaskReceivedNotificationManager() {
        slotCounter = -1;
    }

    public static TaskReceivedNotificationManager getInstance() {
        if (instance == null) {
            instance = new TaskReceivedNotificationManager();
        }
        return instance;
    }

    public void buildNotification(Context applicationContext, TaskSerializable recievedTask) {
        slotCounter = (slotCounter + 1) % MAX_SLOTS;

        int notificationId = slotCounter;
        TaskReceivedNotification taskReceivedNotification;
        taskReceivedNotification = new TaskReceivedNotification(applicationContext, notificationId, recievedTask);
        taskReceivedNotification.buildNotification();
    }
}
