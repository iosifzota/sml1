package com.slm.slm1.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TasksToUpdateManager {

    private static TasksToUpdateManager instance;
    private List<TaskSerializable> tasksToUpdate;
    private final Lock lock;

    public TasksToUpdateManager() {
        this.tasksToUpdate = new ArrayList<>();
        this.lock = new ReentrantLock();
    }

    public static TasksToUpdateManager getInstance() {
        if (instance == null) {
            instance = new TasksToUpdateManager();
        }
        return instance;
    }

    public void addTasksToUpdate(List<TaskSerializable> tasks) {
        lock.lock();
        tasksToUpdate.addAll(tasks);
        lock.unlock();
    }

    public void addTasksToUpdate(TaskSerializable task) {
        lock.lock();
        tasksToUpdate.add(task);
        lock.unlock();
    }

    public List<TaskSerializable> getTasksToUpdate() {
        lock.lock();
        List<TaskSerializable> list = tasksToUpdate;
        tasksToUpdate = new ArrayList<>();
        lock.unlock();
        return list;
    }
}
