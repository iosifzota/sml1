package com.slm.slm1.helper;

import android.util.Log;

import java.io.IOException;
import java.net.Socket;

public class SendTaskRunner implements Runnable {

    private final TaskSerializable taskSerializable;

    public SendTaskRunner(TaskSerializable taskSerializable) {
        this.taskSerializable = taskSerializable;
    }

    @Override
    public void run() {
        Socket socket;
        try {
            socket = new Socket(AppConfig.HOST, 3001);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        ServerConnection serverConnection = new ServerConnection(socket);

        if (serverConnection.writeObject(taskSerializable)) {
            Log.d("msg", "task send - " + taskSerializable.taskName);
        }
        serverConnection.closeConnection();
    }
}
