package com.slm.slm1.helper;

import java.io.Serializable;

public class AddFriendRequest implements Serializable {
    public String senderUsername;
    public String targetUsername;
}
