package com.slm.slm1.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.concurrent.ConcurrentLinkedQueue;


public class ServerConnection {

    private Socket socket;
    private ObjectOutputStream objectOutputStream;
    private ObjectInputStream objectInputStream;
    private ConcurrentLinkedQueue<TaskSerializable> tasksToSend;

    public ServerConnection(Socket socket) {
        this.socket = socket;
        tasksToSend = new ConcurrentLinkedQueue<>();
    }

    public Socket getSocket() {
        return socket;
    }

    public boolean writeObject(Object o) {
        boolean status = true;
        try {
            getObjectOutputStream().writeObject(o);
        } catch (IOException e) {
            e.printStackTrace();
            closeConnection();
            status = false;
        }
        return status;
    }

    public Object readObject() {
        Object output = null;
        try {
            output = getObjectInputStream().readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            closeConnection();
        } catch (IOException e) {
            e.printStackTrace();
            closeConnection();
        } catch (NullPointerException e) {
            e.printStackTrace();
            closeConnection();
        }
        return output;
    }

    public ConcurrentLinkedQueue<TaskSerializable> getTasksToSend() {
        return tasksToSend;
    }

    public ObjectOutputStream getObjectOutputStream() {
        if (objectOutputStream == null) {
            try {
                OutputStream outputStream = socket.getOutputStream();
                objectOutputStream = new ObjectOutputStream(outputStream);
            } catch (IOException e) {
                e.printStackTrace();
                closeConnection();
            }
        }
        return objectOutputStream;
    }

    public ObjectInputStream getObjectInputStream() {
        if (objectInputStream == null) {
            try {
                InputStream inputStream = socket.getInputStream();
                objectInputStream = new ObjectInputStream(inputStream);
            } catch (IOException e) {
                e.printStackTrace();
                closeConnection();
            } catch (NullPointerException e) {
                e.printStackTrace();
                closeConnection();
            }
        }
        return objectInputStream;
    }

    public void closeConnection() {
        try {
            socket.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
