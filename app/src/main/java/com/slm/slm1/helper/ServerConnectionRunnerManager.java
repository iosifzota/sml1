package com.slm.slm1.helper;

public class ServerConnectionRunnerManager {

    private static ServerConnectionRunnerManager instance;
    private final Thread thread;

    private ServerConnectionRunnerManager(Thread thread) {
        this.thread = thread;
    }

    public static ServerConnectionRunnerManager getInstance() {
        if (instance == null) {
            throw new RuntimeException("Should call create first");
        }
        return instance;
    }

    public static ServerConnectionRunnerManager createUnique(Thread thread) {
        if (instance == null) {
            instance = new ServerConnectionRunnerManager(thread);
        }
        return instance;
    }

    public void start() {
        if (!thread.isAlive()) {
            thread.start();
        }
    }
}