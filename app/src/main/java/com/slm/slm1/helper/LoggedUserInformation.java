package com.slm.slm1.helper;

import com.slm.slm1.dao.UserDao;
import com.slm.slm1.model.User;

public class LoggedUserInformation {

    private User mUser;

    private static LoggedUserInformation mInstance;

    private LoggedUserInformation()
    {
        UserDao userDao = AppDatabase.getInstance().userDao();
        mUser = userDao.getUser();
    }

    public static LoggedUserInformation getInstance() {
        if (mInstance == null) {
            mInstance = new LoggedUserInformation();
        }
        return mInstance;
    }

    public User getUser() {
        return mUser;
    }

    public boolean isUserLogged() {
        return mUser != null;
    }
}
