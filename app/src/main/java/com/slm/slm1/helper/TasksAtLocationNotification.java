package com.slm.slm1.helper;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.android.gms.maps.model.LatLng;
import com.slm.slm1.main.TasksAtLocationActivity;
import com.slm.slm1.R;

public class TasksAtLocationNotification {
    private int notificationId;
    private TasksAtLocation tasksAtLocation;
    private Context applicationContext;

    public TasksAtLocationNotification(Context applicationContext, int notificationId, TasksAtLocation tasksAtLocation) {
        this.notificationId = notificationId;
        this.tasksAtLocation = tasksAtLocation;
        this.applicationContext = applicationContext;
    }

    public void buildNotification() {
        String channelId = "TasksAtLocation";
        String channelName = "Tasks at location";
        createNotificationChannel(channelId, channelName);

        String content = tasksAtLocation.getTaskListToString();

        if (content.isEmpty()) {
            return;
        }

        NotificationCompat.Builder builder = new NotificationCompat.Builder(applicationContext, channelId)
                .setContentTitle("Task list")
                .setContentText(content)
                // make this notification ongoing (sticky)
                .setOngoing(true)
                .setSmallIcon(R.drawable.tracking_enabled)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(content));

        Intent intent = new Intent(applicationContext, TasksAtLocationActivity.class);
        LatLng location = new LatLng(tasksAtLocation.location.lat, tasksAtLocation.location.lg);
        intent.putExtra("location", location);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, 0);
        builder.setContentIntent(pendingIntent);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(applicationContext);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(notificationId, builder.build());
    }

    private void createNotificationChannel(String id, String name) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_DEFAULT);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = applicationContext.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
}
