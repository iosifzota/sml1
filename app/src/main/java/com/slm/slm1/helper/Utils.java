package com.slm.slm1.helper;

import com.google.android.gms.maps.model.LatLng;

public class Utils {

    public static LatLng distanceToCoordinates(double kilometers) {
        // calculate latitude: 1deg = 110.574 km
        double latititude = kilometers/110.574;

        // calculate longitude: 1deg = 111.320 * cos(latititude ...in radians);
        double latititudeInRadians = (latititude * Math.PI) / 180;
        if (Math.abs(latititudeInRadians - Math.toRadians(latititude)) > 0.0001)
            throw new AssertionError();
        double longitude = kilometers / (111.320 * Math.cos(latititudeInRadians));

        return new LatLng(latititude, longitude);
    }
}
