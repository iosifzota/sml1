package com.slm.slm1.helper;

import android.content.Context;

import com.slm.slm1.main.MapActivity;

public class UpdateNotificationListener implements Listener {

    private Context applicationContext;

    public UpdateNotificationListener(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void update() {
        MapActivity.updateTasksNotification(applicationContext, true);
    }
}
