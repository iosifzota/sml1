package com.slm.slm1.helper;

import com.slm.slm1.model.Location;
import com.slm.slm1.model.TaskAndTaskDetails;
import com.slm.slm1.dao.TaskDao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TasksAtLocation {

    public Location location;
    private final Date deadline;

    public TasksAtLocation(Location location, Date deadline) {
        this.location = location;
        this.deadline = deadline;
    }

    public List<TaskAndTaskDetails> getTaskList() {
        TaskDao taskDao = AppDatabase.getInstance().taskDao();
        List<TaskAndTaskDetails> tasks;
        tasks = taskDao.getTaskAndTaskDetailsOnLocationOnDate(location.id, deadline);

        if (tasks == null) {
             tasks = new ArrayList<>();
        }
        return tasks;
    }

    public String getTaskListToString() {
        List<TaskAndTaskDetails> tasks = getTaskList();

        StringBuilder content = new StringBuilder();

        int i;
        for (i = 0; i < tasks.size() - 1; i++) {
            TaskAndTaskDetails task = tasks.get(i);
            content.append(task.task.name).append("\n");
        }
        if (i < tasks.size()) {
            TaskAndTaskDetails task = tasks.get(i);
            content.append(task.task.name);
        }

        return content.toString();
    }
}
