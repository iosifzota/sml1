package com.slm.slm1.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class TasksToSaveManager {

    private static TasksToSaveManager instance;
    private List<TaskSerializable> tasksToSave;
    private final Lock lock;

    public TasksToSaveManager() {
        this.tasksToSave = new ArrayList<>();
        this.lock = new ReentrantLock();
    }

    public static TasksToSaveManager getInstance() {
        if (instance == null) {
            instance = new TasksToSaveManager();
        }
        return instance;
    }

    public void addTasksToSave(List<TaskSerializable> tasks) {
        lock.lock();
        tasksToSave.addAll(tasks);
        lock.unlock();
    }

    public void addTasksToSave(TaskSerializable task) {
        lock.lock();
        tasksToSave.add(task);
        lock.unlock();
    }

    public List<TaskSerializable> getTasksToSave() {
        lock.lock();
        List<TaskSerializable> list = tasksToSave;
        tasksToSave = new ArrayList<>();
        lock.unlock();
        return list;
    }
}
