package com.slm.slm1.helper;

import android.content.Context;

import com.slm.slm1.dao.LocationDao;
import com.slm.slm1.model.Task;
import com.slm.slm1.dao.TaskDao;
import com.slm.slm1.model.Location;
import com.slm.slm1.model.TaskAndTaskDetails;
import com.slm.slm1.model.TaskDetails;
import com.slm.slm1.dao.TaskDetailsDao;

import java.util.ArrayList;
import java.util.List;

public class DatabaseUtil {

    public static void addReceivedTask(TaskSerializable taskSerializable) {
        // insert location
        LocationDao locationDao = AppDatabase.getInstance().locationDao();
        long locationid = -1;
        Location location = locationDao.find(taskSerializable.locationLat, taskSerializable.locationLg);
        if (location == null) {
            location = new Location(taskSerializable.locationLat, taskSerializable.locationLg);
            locationid = locationDao.insert(location);
        } else {
            locationid = location.id;
        }

        // insert task
        TaskDao taskDao = AppDatabase.getInstance().taskDao();
        Task task = new Task();
        task.name = taskSerializable.taskName;
        task.completionStatus = taskSerializable.completionStatus;
        task.deadline = taskSerializable.deadline;
        task.locationfk = (int)locationid;
        task.serverTaskId = taskSerializable.localTaskId;
        long taskid = taskDao.insertTask(task);

        // insert task details
        TaskDetailsDao taskDetailsDao = AppDatabase.getInstance().taskDetailsDao();
        TaskDetails taskDetails = new TaskDetails();
        taskDetails.notes = taskSerializable.taskNotes;
        taskDetails.taskfk = (int) taskid;
        taskDetailsDao.insertTaskDetails(taskDetails);

        Context applicationContext = AppDatabase.getContext();
        TaskReceivedNotificationManager
                .getInstance()
                .buildNotification(applicationContext, taskSerializable);
    }

    public static List<TaskSerializable> convertToSerializable(List<TaskAndTaskDetails> tasksToDelete) {
        List<TaskSerializable> result = new ArrayList<>();

        if (tasksToDelete != null && !tasksToDelete.isEmpty()) {
            for (TaskAndTaskDetails task : tasksToDelete) {
                result.add(DatabaseUtil.convertToSerializable(task));
            }
        }
        return result;
    }

    private static TaskSerializable convertToSerializable(TaskAndTaskDetails task) {
        LocationDao locationDao = AppDatabase.getInstance().locationDao();
        Location location = locationDao.getLocation(task.task.locationfk);

        TaskSerializable taskSerializable = new TaskSerializable();
        taskSerializable.taskName = task.task.name;
        taskSerializable.taskNotes = task.taskDetails.notes;
        taskSerializable.locationLat = location.lat;
        taskSerializable.locationLg = location.lg;
        taskSerializable.completionStatus = task.task.completionStatus;
        taskSerializable.targetUsername = AuthInformation.getInstance().getUserCredentials().username;
        taskSerializable.localTaskId = task.task.serverTaskId;
        taskSerializable.deadline = task.task.deadline;

        return taskSerializable;
    }

    public static void setServerIdOfTasks(List<TaskSerializable> tasksToSave, List<Integer> savedTasksId) {
        TaskDao taskDao = AppDatabase.getInstance().taskDao();

        for (int i = 0; i < tasksToSave.size(); i++) {
            TaskSerializable taskSerializable = tasksToSave.get(i);
            Task task = taskDao.getTask(taskSerializable.localTaskId);
            task.serverTaskId = savedTasksId.get(i);
            taskDao.update(task);
        }
    }
}
