package com.slm.slm1.helper;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.android.gms.maps.model.LatLng;
import com.slm.slm1.main.TasksAtLocationActivity;
import com.slm.slm1.R;

public class TaskReceivedNotification {

    private Context applicationContext;
    private int notificationId;
    private TaskSerializable receivedTask;

    public TaskReceivedNotification(Context applicationContext, int notificationId, TaskSerializable receivedTask) {
        this.applicationContext = applicationContext;
        this.notificationId = notificationId;
        this.receivedTask = receivedTask;
    }

    public void buildNotification() {
        // create notification channel
        String channelId = receivedTask.taskName + receivedTask.targetUsername;
        String channelName = receivedTask.taskName;
        createNotificationChannel(channelId,  channelName);

        String content = channelName + " - " + receivedTask.targetUsername;

        // setup notification builder
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(applicationContext, channelId);
        notificationBuilder
                .setSmallIcon(R.drawable.tracking_enabled)
                .setContentTitle("Received task")
                .setContentText(content)
                .setAutoCancel(true);

        // setup pending intent for notification
//        LatLng location = new LatLng(receivedTask.locationLat, receivedTask.locationLg);
//        Intent intent = new Intent(applicationContext, TasksAtLocationActivity.class);
//        intent.putExtra("location", location);
//        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(applicationContext, 0, intent, 0);
//        notificationBuilder.setContentIntent(pendingIntent);

        // build notification
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(applicationContext);
        notificationManager.notify(notificationId, notificationBuilder.build());
    }

    private void createNotificationChannel(String id, String name) {
        NotificationChannel notificationChannel;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationChannel = new NotificationChannel(id, name, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = applicationContext.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
}
