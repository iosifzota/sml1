package com.slm.slm1.helper;

import android.content.Context;

import com.slm.slm1.model.Location;

import java.util.Calendar;
import java.util.Date;

public class TasksAtLocationNotificationManager {

    private static TasksAtLocationNotificationManager instance;
    public static final int NOTIFCATION_ID = TaskReceivedNotificationManager.MAX_SLOTS;

    private TasksAtLocationNotificationManager() {
    }

    public static TasksAtLocationNotificationManager getInstance() {
        if (instance == null) {
            instance = new TasksAtLocationNotificationManager();
        }
        return instance;
    }

    public void buildNotification(Context applicationContext, Location location) {
        // show notification for today only
        Date deadline;
        Calendar calendar = Calendar.getInstance();
        deadline = new Date(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));

        TasksAtLocation tasksAtLocation = new TasksAtLocation(location, deadline);
        TasksAtLocationNotification notification;

        notification = new TasksAtLocationNotification(applicationContext, NOTIFCATION_ID, tasksAtLocation);
        notification.buildNotification();
    }
}
