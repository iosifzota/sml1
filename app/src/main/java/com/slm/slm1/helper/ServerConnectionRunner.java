package com.slm.slm1.helper;

import android.content.Context;
import android.util.Log;

import androidx.annotation.RestrictTo;
import androidx.room.Database;

import java.io.IOException;
import java.net.Socket;
import java.util.List;

public class ServerConnectionRunner implements Runnable {

    private Context applicationContext;

    public ServerConnectionRunner(Context applicationContext) {
        this.applicationContext = applicationContext;
    }

    @Override
    public void run() {

        for (;;) {
            Socket socket;
            try {
                socket = new Socket(AppConfig.HOST, 3002);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            ServerConnection serverConnection = new ServerConnection(socket);

            PollRequest pollRequest = new PollRequest();
            pollRequest.senderUsername = AuthInformation.getInstance().getUserCredentials().username;

            if (serverConnection.writeObject(pollRequest)) {
                List<TaskSerializable> response = (List<TaskSerializable>) serverConnection.readObject();

                if (response != null) {
                    for (TaskSerializable taskSerializable : response) {
                        Log.d("msg", "received task - " + taskSerializable.taskName);
                        DatabaseUtil.addReceivedTask(taskSerializable);
                    }
                }
            }

            serverConnection.closeConnection();

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
                throw new RuntimeException("ServerConnectionRunner has stopped.");
            }
        }
    }
}
