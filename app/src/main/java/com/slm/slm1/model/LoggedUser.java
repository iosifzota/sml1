package com.slm.slm1.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class LoggedUser {
    @PrimaryKey(autoGenerate = true)
    public int userId;
}
