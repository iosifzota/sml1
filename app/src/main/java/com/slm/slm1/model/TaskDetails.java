package com.slm.slm1.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class TaskDetails {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "notes")
    public String notes;

    @ColumnInfo(name = "taskfk")
    public int taskfk;
}
