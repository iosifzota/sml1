package com.slm.slm1.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class RecordWithValues {
    @Embedded public Record record;
    @Relation(
            parentColumn = "id",
            entityColumn = "recordfk"
    )
    public List<Value> values;
}
