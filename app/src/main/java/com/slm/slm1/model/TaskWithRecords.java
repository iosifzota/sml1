package com.slm.slm1.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class TaskWithRecords {
    @Embedded public Task task;
    @Relation(
            parentColumn = "id",
            entityColumn = "taskfk"
    )
    public List<Record> records;
}
