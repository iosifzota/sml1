package com.slm.slm1.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Value {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "str")
    public String str;

    @ColumnInfo(name = "recordfk")
    public int recordfk;
}
