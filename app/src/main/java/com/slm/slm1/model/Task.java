package com.slm.slm1.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Date;

@Entity
public class Task {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "name")
    public String name;

    @ColumnInfo(name = "completion_status")
    public boolean completionStatus;

    @ColumnInfo(name = "deadline")
    public Date deadline;

    @ColumnInfo(name = "locationfk")
    public int locationfk;

    @ColumnInfo(name = "server_task_id")
    public int serverTaskId;

    @Override
    public String toString() {
        return name;
    }
}
