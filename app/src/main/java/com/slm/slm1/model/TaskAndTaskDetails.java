package com.slm.slm1.model;

import androidx.room.Embedded;
import androidx.room.Relation;

public class TaskAndTaskDetails {
    @Embedded public Task task;
    @Relation(
            parentColumn = "id",
            entityColumn = "taskfk"
    )
    public TaskDetails taskDetails;

    @Override
    public String toString() {
        return task != null ? task.toString() : "";
    }
}
