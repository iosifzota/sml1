package com.slm.slm1.model;

import androidx.room.Embedded;
import androidx.room.Relation;

public class LocationAndLocationDetails {
    @Embedded public Location location;
    @Relation(
            parentColumn = "id",
            entityColumn = "locationfk"
    )
    public LocationDetails locationDetails;
}
