package com.slm.slm1.model;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.Relation;

import java.util.List;

@Entity
public class LocationWithTasks {
    @Embedded public Location location;
    @Relation(
            parentColumn = "id",
            entityColumn = "locationfk"
    )
    public List<Task> tasks;
}
