package com.slm.slm1.model;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class TaskWithRecordsAndValues {
    @Embedded public Task task;
    @Relation(
            entity = Record.class,
            entityColumn = "taskfk",
            parentColumn = "id"
    )
    public List<RecordWithValues> records;
}
