package com.slm.slm1.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Location {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "lat")
    public double lat;

    @ColumnInfo(name = "lg")
    public double lg;

    public Location(double lat, double lg) {
        this.lat = lat;
        this.lg = lg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Location location = (Location) o;

        if (id != location.id) return false;
        if (Double.compare(location.lat, lat) != 0) return false;
        return Double.compare(location.lg, lg) == 0;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        temp = Double.doubleToLongBits(lat);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lg);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
